import React, {useState} from 'react';
import Home from './pages/Home';
import ProductList from './pages/ProductList';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import {UserProvider} from './UserContext'
import Navbar from './components/Navbar'
import ProductDetails from './pages/ProductDetails'
import Cart from './components/Cart';
//import Cartist from './components/CartList';




import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  //const UserContext = React.createContext()

    const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

    const unsetUser = () => {
      localStorage.clear()
    }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Navbar/>
     
      <Router>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/products" element={<ProductList/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/productDetails/:productId" element={<ProductDetails/>} />
            <Route path="/cart" element={<Cart/>} />

          </Routes>
      </Router>
      
      
    </UserProvider>
  );
}   

export default App;