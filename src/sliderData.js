export const sliderItems = [
	{
		id: 1,
		img: "https://www.udiscovermusic.com/wp-content/uploads/2020/08/Abbey-Road-820x820.jpg",
		title: "Abbey Road",
		artist: "The Beatles",
		desc: "The 50th anniversary editions of Abbey Road",
		bg: "f5fafd"
	},
	{
		id: 2,
		img: "https://media.pitchfork.com/photos/60a6da93853a96fc7df320ba/1:1/w_320,c_limit/MOTHER%20NATURE%20ALBUM%20COVER%20(1).png",
		title: "Mother Nature",
		artist: "Angélique Kidjo",
		desc: "2021 Grammy Award for Best Global Music Album",
		bg: "fcf1ed"
	},
	{
		id: 3,
		img: "https://cdns-images.dzcdn.net/images/cover/92a024220a9532489c75c9d994835697/500x500.jpg",
		title: "Thriller",
		artist: "Michael Jackson",
		desc: "Best Selling Album of All Time",
		bg: "f5fafd"
	},
]