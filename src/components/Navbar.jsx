import React, {useContext, useState, useEffect} from 'react'
import styled from 'styled-components'
import { Search, Album, ShoppingCartOutlined, } from "@mui/icons-material";
import { Badge } from '@mui/material';
//import {Link} from 'react-router-dom'
import UserContext from '../UserContext';

const Container = styled.div`
	height: 60px;
	margin-bottom: 7px
`

const Wrapper = styled.div`
	padding: 10px 20px;
	display: flex;
	align-items: center;
	justify-content: space-between;
`

const Left = styled.div`
	flex: 2;
	display: flex;
	align-items: center;
`
const Center = styled.div`
	flex: 10;
	display: flex;
	align-items: center;
`
const Right = styled.div`
	flex: 4;
	display: flex;
	align-items: center;
	justify-content: flex-end;
`

const Logo = styled.span`
	font-size: 25px;
`
const SearchContainer = styled.div`
	border: 0.5px solid lightgrey;
	display: flex;
	align-items: center;
	margin-left: 25px;
	padding: 5px;
	flex:1
`

const Input = styled.input`
	border: none;
	flex: 1

`

const Menu = styled.div`
	font-size: 14px;
	margin-left: 25px;
	cursor: pointer;
	color: black;
`

const Navbar = () => {

	const[allCart, setAllCart] = useState([])
	const {user} = useContext(UserContext);

	//const navigate = useNavigate();
			//const { productId } = useParams();
			
			
				const fetchData = () => {
				fetch(`https://arcane-river-38155.herokuapp.com/carts/cart`, {
					headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
				})

				.then(res => res.json())
				.then(data => {
					

					setAllCart(data)

				})
				}
			useEffect(() => {
				fetchData()
			} )

	return (
		<Container>
			<Wrapper>
				<Left>
					<Logo>&#60;K <Album style={{fontSize: 60}}/>&#62;</Logo>
					
				</Left>
				<Center>
					<SearchContainer>
						<Input placeholder="Artist, Single, Album"/>
						<Search style={{color: "gray", fontSize: 25}}/>
					</SearchContainer>
				</Center>
				<Right>


					{(user.isAdmin === true && user.accessToken !== null) ?
						<a href="/logout"><Menu>Logout</Menu></a>

						:
				
					(user.accessToken === null) ?

					<>
					<a href="/"><Menu>Home</Menu></a>
					<a href="/products"><Menu>Product</Menu></a>
					<a href="/about"><Menu>About</Menu></a>
					<a href="/register"><Menu>Register</Menu></a>
					<a href="/login"><Menu>Login</Menu></a>
					</>
					
					: 

					<>
					<a href="/"><Menu>Home</Menu></a>
					<a href="/products"><Menu>Product</Menu></a>
					<a href="/about"><Menu>About</Menu></a>
					<a href="/logout"><Menu>Logout</Menu></a>
					<a href="/cart">
						<Menu>
							<Badge badgeContent={allCart.length} color="primary">
							  <ShoppingCartOutlined color="action" />
							</Badge>
						</Menu>
					</a>
					</>
					}


					
				


				</Right>
			</Wrapper>
		</Container>
	)
}

export default Navbar