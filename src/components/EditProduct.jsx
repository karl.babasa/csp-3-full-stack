import React, { useState, useContext } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function EditProduct({product, fetchData}) {
	
	const {user} = useContext(UserContext);

	//state for courseId for the fetch URL
	const [productId, setProductId] = useState('');


	//Forms state
	//Add state for the forms of course
	const [name, setName] = useState('');
	const [description , setDescription] = useState('');
	const [price , setPrice] = useState('');
	const [artist , setArtist] = useState('');
	const [img , setImg] = useState('');

	//state for editCourse Modals to open/close
	const [showEdit, setShowEdit] = useState(false)

	//Function for opening the modal
	const openEdit = (productId) => {
		//to still get the actual data from the form
		fetch(`https://arcane-river-38155.herokuapp.com/products/${productId}`, {
		headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('accessToken')}`},
			})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			//populate all the input values with course info that we fetched
			setProductId(data._id);
			setName(data.name);
			setArtist(data.artist);
			setDescription(data.description);
			setPrice(data.price);
			setImg(data.img);
			console.log()
		})
		//Then, open the modal
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');		
		setPrice(0)
		setArtist('');
		setImg('');
	}

	//function to update the course
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://arcane-river-38155.herokuapp.com/products/${productId}`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				artist: artist,
				description: description,
				price: price,
				img: img
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Successfully Updated'
				})
				closeEdit();
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				fetchData();
			}
		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Update</Button>

		{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Album</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Album's Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Artist</Form.Label>
							<Form.Control type="text" value={artist} onChange={e => setArtist(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Album Cover</Form.Label>
							<Form.Control placeholder="Use online image URL" type="test" value={img} onChange={e => setImg(e.target.value)}/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}