import React from 'react'
import styled from "styled-components"
//import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import { SearchOutlined } from "@mui/icons-material";

const Info = styled.div`
	opacity: 0;
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	background-color: rgba(0,0,0,0.2);
	z-index: 3;
	display:flex;
	align-items: center;
	justify-content: center;
	transition: all 0.5s ease;
`

const Container = styled.div`
	flex: 1;
	margin: 5px;
	min-width: 280px;
	height: 350px;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #d0eeee;
	position: relative;
	flex-direction: column;

	&:hover ${Info}{
		opacity: 1;
	}
`

const Image = styled.img`
	height: 75%;
	z-index: 2;
`

const Icon = styled.div`
	width: 40px;
	height: 40px;
	border-radius: 50%;
	background-color: white;
	display: flex;
	align-items: center;
	justify-content: center;
	transition: all 0.3s ease;
	cursor: pointer;

	&:hover{
		background-color: #e9f5f5;
		transform: scale(1.1)
	}
`

const Circle = styled.div`
	width: 200px;
	height: 200px;
	border-radius: 50%;
	background-color: white;
	position: absolute;

`;

const Details = styled.span`
	
`


const Product = ({productProp}) => {
	console.log(productProp)
	const {_id, name, price, img, artist} = productProp;
	return (
		<Container>
			<Circle/>
				<Image src={img}/>
					<Info>
						<Icon>
							<Link to={`/productDetails/${_id}`}><SearchOutlined/></Link>
						</Icon>
					</Info>
				<Details>
					{name}
				</Details>
				<Details style={{fontSize: 10}}>
					{artist}
				</Details>
				<Details style={{fontSize: 8}}> 
					₱ {price}.00
				</Details>
		</Container>
	)
}

export default Product