import React, {useContext, useState, useEffect} from 'react';
import styled from "styled-components";
import UserView from './UserView';
import AdminView from './AdminView';
import UserContext from '../UserContext';
import Navbar from './Navbar';

const Container = styled.div`
	padding: 20px;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
`





const Products = () => {

	const {user} = useContext(UserContext);

	const[allProduct, setAllProduct] = useState([])

	const fetchData = () => {
		fetch('https://arcane-river-38155.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {

			setAllProduct(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])


	return (
		<Container>

		{(user.isAdmin === true) ?

			<AdminView productData = {allProduct} fetchData = {fetchData}/>

			:
			<>
			<UserView productData = {allProduct} fetchData = {fetchData}/>
			
			</>
		}

		
		</Container>
	)
}

export default Products