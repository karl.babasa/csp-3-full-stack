import React from 'react'
import styled from "styled-components"
import { Album, Facebook, GitHub, Instagram, YouTube, Room, Phone, MailOutline } from "@mui/icons-material";

const Container = styled.div`
	display: flex;
`

const Left = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	padding: 20px;
`

const Center = styled.div`
	flex: 1;
	padding: 20px;
`

const Title = styled.h3`
	margin-bottom: 30px;
`

const List = styled.ul`
	margin: 0;
	padding: 0;
	list-style: none;
	display: flex;
	flex-wrap: wrap;
`

const ListItem = styled.li`
	width: 50%;
	margin-bottom: 10px;
`

const Right = styled.div`
	flex: 1;
	padding: 20px;
`

const Logo = styled.h1`
	font-size: 20px
`
const Desc = styled.p`
	margin: 20px 0px;
`
const SocialContainer = styled.div`
	display: flex;

`
const SocialIcon = styled.div`
	width: 40px;
	height: 40px;
	border-radius: 50%; 
	color: white;
	background-color: #${props => props.color};
	display: flex;
	align-items: center;
	justify-content: center;
	margin-right: 20px;
`

const ContactItem = styled.div`
	margin-bottom: 20px;
	display: flex;
	align-item: center;
`



const Footer = () => {
	return (
		<Container>
			<Left>
				<Logo>&#60;K <Album style={{fontSize: 40}}/>&#62;</Logo>
				<Desc>
					Full-Stack Capstone Project (E-Commerce)
				</Desc>
				<SocialContainer>
					<SocialIcon color="3B5999">
						<Facebook/>
					</SocialIcon>

					<SocialIcon color="E4405F">
						<GitHub/>
					</SocialIcon>

					<SocialIcon color="55ACEE">
						<Instagram/>
					</SocialIcon>

					<SocialIcon color="E60023">
						<YouTube/>
					</SocialIcon>
				</SocialContainer>
			</Left>

			<Center>
				<Title>Useful Links</Title>
				<List>
					<ListItem>zuitt.co</ListItem>
					<ListItem>My Account</ListItem>
					<ListItem>Terms and Conditions</ListItem>
					<ListItem>Help</ListItem>
					<ListItem>Shipping Rates</ListItem>
				</List>
			</Center>

			<Right>
				<Title>Contact</Title>
				<ContactItem>
					<Room style={{marginRight: "10px"}}/>322 Hidden Leaf Village, Land of The Fire
				</ContactItem>

				<ContactItem>
					<Phone style={{marginRight: "10px"}}/>+63 123 456 7895
				</ContactItem>

				<ContactItem>
					<MailOutline style={{marginRight: "10px"}}/>sample@email.com
				</ContactItem>
			</Right>
		</Container>
	)
}

export default Footer