import React, { useState, useEffect, useContext } from 'react';
//import UserContext from '../UserContext';
import styled from 'styled-components'
import { Add, Remove, Delete } from "@mui/icons-material";
//import { /*useNavigate,*/ Link } from 'react-router-dom';
//import CartView from './CartView'
import UserContext from '../UserContext';
import Cart from '../components/Cart';

const ProductAmountCountainer = styled.div`
	display: flex;
	align-items: center;
`
const ProductAmount = styled.div`

	font-size: 24px;
	margin: 5px;
`
const ProductPrice = styled.div`
	font-size: 30px;
	font-weight: 200;
`

const Hr = styled.hr`
	background-color: #eee;
	border: none;
	height: 1px;
`
const Product = styled.div`
	display:flex;
	justify-content: space-between;
`
const ProductDetail = styled.div`
	flex: 2;
	display: flex;
	align-items: center;
	justify-content: center;
`
const Image = styled.img`
	width: 250px;
`
const Details = styled.div`
	padding: 20px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
`
const ProductArtist = styled.span``
const ProductInfo = styled.span``
const PriceDetail = styled.div`
	flex: 1;
	display:flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

`

const Button = styled.button``


const MyCartList = ({cartProp}) => {

		const {productId, productName, productDescription, productPrice, productArtist, quantity, productImg} = cartProp;

		console.log(cartProp)

		class fruitCollection extends Array {
    sum(key) {
        return this.reduce((a, b) => a + (b[key] || 0), 0);
    }
}
const fruit = new fruitCollection(cartProp);

console.log(fruit.sum('productPrice'));

		const totalAmount = 0
		const [total, setTotal] = useState(quantity)

			function increase() {
				setTotal(total+1)
			}

			function decrease() {
				setTotal(total-1)
			}

			/*useEffect(() => {
				if (total <= 1) {
					document.getElementById("theButton").disabled = true;
				} else {
					document.getElementById("theButton").disabled = false;
				}
			}, [total])*/

		return (

		<>
		<Product>
			<ProductDetail>
				<Image src={productImg}/>
				<Details>
					<ProductArtist><b>Album:</b> {productName} </ProductArtist>
					<ProductArtist><b>Artist:</b> {productArtist} </ProductArtist>
					<ProductInfo><b>Detail:</b> {productDescription} </ProductInfo>
				</Details>
			</ProductDetail>
			<PriceDetail>
				<ProductAmountCountainer>
					<Button onClick = {decrease}><Remove/></Button>
					<ProductAmount>{total}</ProductAmount>
					<Button onClick = {increase}><Add/></Button>
				</ProductAmountCountainer>
				<ProductPrice>{total*productPrice}</ProductPrice>
				<Delete style={{fontSize: 50}}/>
			</PriceDetail>
		</Product>
		<Hr/>
		
		</>
	)

}




export default MyCartList