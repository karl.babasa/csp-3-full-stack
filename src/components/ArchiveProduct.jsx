import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ArchiveProduct({product, isAvailable, fetchData}) {

	const {user} = useContext(UserContext);

	const archiveToggle = (productId) => {
		fetch(`https://arcane-river-38155.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
		})

		.then(res => res.json())
		.then(data => {
			
			if(data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Album is now Unavailable'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}


	const restoreToggle = (productId) => {
		fetch(`https://arcane-river-38155.herokuapp.com/products/${productId}/restore`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
		})

		.then(res => res.json())
		.then(data => {
			
			if(data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Album is now Available'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}
 

	return(
		<>
			{isAvailable ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => restoreToggle(product)}>Unarchive</Button>

			}
		</>

		)
}