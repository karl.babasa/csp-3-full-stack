import React, {useState, useEffect} from 'react'
import MyCartList from '../components/MyCartList'

export default function CartView({cartData}) {
	
	const [cart, setCart] = useState([])

	useEffect(() => {
		const cartArr = cartData.map(cart => {
			//only render the active courses
			return (
					<MyCartList cartProp={cart} key={cart._id}/>
			)
				
			
		})
		//set the courses to the result of our ma function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
		setCart(cartArr)

	}, [cartData])
	
	return (
			<>
				{cart}
			</>
		)
}