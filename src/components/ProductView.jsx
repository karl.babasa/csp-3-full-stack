import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import styled from 'styled-components'
import { Add, Remove } from "@mui/icons-material";

const Container = styled.div`
	
`
const Wrapper = styled.div`
	padding: 20px;
`
const Title = styled.h1`
	font-weight: 300;
	text-align: center;

`
const Top = styled.div`
	justify-content: space-between;
	display: flex;
	align-items: center;
	padding: 20px;
`
const TopButton = styled.button`
	padding: 10px;
	font-weight: 600;
	cursor: pointer;
	border: ${props =>props.type === "filled" && "none"};
	background-color: ${props =>props.type === "filled" ? "black" : "transparent"};
	color: ${props =>props.type === "filled" && "white"};

`

const TopTexts = styled.div``
/*const TopText = styled.span`
	text-decoration: underline;
	cursor: pointer;
`*/

const Bottom = styled.div`
	display: flex;
	justify-content: space-between;
`;
const Info = styled.div`
	flex: 3;
`
const Summary = styled.div`
	flex: 1;
`
const Product = styled.div`
	display:flex;
	justify-content: space-between;
`
const ProductDetail = styled.div`
	flex: 2;
	display: flex;
	align-items: center;
	justify-content: center;
`
const Image = styled.img`
	width: 250px;
`
const Details = styled.div`
	padding: 20px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
`
const ProductArtist = styled.span``
const ProductInfo = styled.span``
const PriceDetail = styled.div`
	flex: 1;
	display:flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	border-left: 1px solid black
	

`

const ProductAmountCountainer = styled.div`
	display: flex;
	align-items: center;
`
const ProductAmount = styled.div`

	font-size: 24px;
	margin: 5px;
`
const ProductPrice = styled.div`
	font-size: 30px;
	font-weight: 200;

`

const Button = styled.button``

const ProductView = () => {

	const navigate = useNavigate();
	const { productId } = useParams();
	console.log(productId)
	useEffect(() => {

		fetch(`https://arcane-river-38155.herokuapp.com/products/${productId}`, {
			headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
		})

		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setArtist(data.artist)
			setDescription(data.description)
			setPrice(data.price)
			setImg(data.img)

			console.log(data)
		})

	})

	const { user } = useContext(UserContext);


	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [img, setImg] = useState('')
	const [artist, setArtist] = useState('')

	const [total, setTotal] = useState(1)

	function increase() {
		setTotal(total+1)
	}

	function decrease() {
		setTotal(total-1)
	}

	useEffect(() => {
		if (total === 1) {
			document.getElementById("theButton").disabled = true;
		} else {
			document.getElementById("theButton").disabled = false;
		}
	}, [total])




	function addToCart(e) {
		e.preventDefault()

		fetch('https://arcane-river-38155.herokuapp.com/carts/add', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
			body: JSON.stringify({
				productId: productId,
				quantity: total,
				productName: name,
				productPrice: price,
				productArtist: artist,
				userId: user.id,
				productDescription: description,
				productImg: img

			})

		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			Swal.fire({
			title: "Yay!",
			icon: 'success',
			text: `${total} ${name} by ${artist} Successfully Added to Cart`
			})
			
			navigate('/products')

		})
	}

	function backToProduct() {
		navigate('/products')
	}

	return (
		<Container>
			<Container>
				<Wrapper>
					<Title>{name}</Title>
					<Top>
					<TopButton onClick={backToProduct}>CONTINUE SHOPPING</TopButton>
					<TopTexts>

					</TopTexts>
					<TopButton type="filled" onClick={addToCart}>ADD TO CART</TopButton>
					</Top>
					<Bottom>
						<Info>
							<Product>
								<ProductDetail>
									<Image src={img}/>
									<Details>
										<ProductArtist><b>Artist:</b> {artist}</ProductArtist>
										<ProductInfo><b>Detail:</b> {description}</ProductInfo>
									</Details>
								</ProductDetail>
								<PriceDetail>
									<ProductAmountCountainer>
										<Button id="theButton" onClick={decrease}><Remove/></Button>
										<ProductAmount>{total}</ProductAmount>
										<Button onClick={increase}><Add/></Button>
									</ProductAmountCountainer>
									<ProductPrice>₱{price*total}.00</ProductPrice>
								</PriceDetail>
							</Product>	
						</Info>
						<Summary></Summary>
					</Bottom>
				</Wrapper>
			</Container>
		</Container>
	)
}

export default ProductView