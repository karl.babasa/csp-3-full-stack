import React, { useState, useEffect, useContext } from 'react';
import CartView from './CartView'
//import MyCartList from '../components/MyCartList'
import UserContext from '../UserContext';
//import { useParams/*, useNavigate, Link */} from 'react-router-dom';
import Navbar from './Navbar';

const CartList = () => {

	const[allCart, setAllCart] = useState([])
	const {user} = useContext(UserContext);

	//const navigate = useNavigate();
			//const { productId } = useParams();
			
			
				const fetchData = () => {
				fetch(`https://arcane-river-38155.herokuapp.com/carts/cart`, {
					headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
				})

				.then(res => res.json())
				.then(data => {
					

					setAllCart(data)

				})
				}
			useEffect(() => {
				fetchData()
			} )

			console.log(allCart)

	return (
		<div>
			<CartView cartData = {allCart} fetchData = {fetchData}/>
		</div>
	)
}

export default CartList