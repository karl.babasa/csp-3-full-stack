import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components'
import { useNavigate } from 'react-router-dom'
import CartList from '../components/CartList'	
import { Add, Remove, Delete } from "@mui/icons-material";
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const ProductAmountCountainer = styled.div`
	display: flex;
	align-items: center;
`
const ProductAmount = styled.div`

	font-size: 24px;
	margin: 5px;
`
const ProductPrice = styled.div`
	font-size: 30px;
	font-weight: 200;
`

const Hr = styled.hr`
	background-color: #eee;
	border: none;
	height: 1px;
`
const Product = styled.div`
	display:flex;
	justify-content: space-between;
`
const ProductDetail = styled.div`
	flex: 2;
	display: flex;
	align-items: center;
	justify-content: center;
`
const Image = styled.img`
	width: 250px;
`
const Details = styled.div`
	padding: 20px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
`
const ProductArtist = styled.span``
const ProductInfo = styled.span``
const PriceDetail = styled.div`
	flex: 1;
	display:flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;`



const Container = styled.div`
	
`
const Wrapper = styled.div`
	padding: 20px;
`
const Title = styled.h1`
	font-weight: 300;
	text-align: center;

`
const Top = styled.div`
	justify-content: space-between;
	display: flex;
	align-items: center;
	padding: 20px;
`
const TopButton = styled.button`
	padding: 10px;
	font-weight: 600;
	cursor: pointer;
	border: ${props =>props.type === "filled" && "none"};
	background-color: ${props =>props.type === "filled" ? "black" : "transparent"};
	color: ${props =>props.type === "filled" && "white"};

`
const TopTexts = styled.div`
`
const TopText = styled.span`
	text-decoration: underline;
	cursor: pointer;
`

const Bottom = styled.div`
	display: flex;
	justify-content: space-between;
`;
const Info = styled.div`
	flex: 3;
`
const Summary = styled.div`
	flex: 1;
	border: 0.5px solid lightgray;
	border-radius: 10px;
	padding: 20px;
	height: 70vh;
`
const SummaryTitle = styled.h1`
	font-weight: 200;
`
const SummaryItem = styled.div`
	margin: 30px 0px;
	display: flex;
	justify-content: space-between;
	font-weight: ${props => props.type === "total" && "500"};
	font-size: ${props => props.type === "total" && "24px"};
`
const SummaryItemText = styled.span`
	
`
const SummaryItemPrice = styled.span`
	
`
const Button = styled.button`
	width: 100%;
	padding: 10px;
	background-color: black;
	color: white;
	font-weight: 600;
`

const ButtonDelete = styled.button`
	
`

const Cart = () => {

	const[allCart, setAllCart] = useState([])
	const[quantity, setQuantity] = useState([])
	const {user} = useContext(UserContext);
			
			
				const fetchData = () => {
				fetch(`https://arcane-river-38155.herokuapp.com/carts/cart`, {
					headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
				})

				.then(res => res.json())
				.then(data => {
					

					setAllCart(data)

				})
				}
			useEffect(() => {
				fetchData()
			} )
		
	const navigate = useNavigate();

	function backToProduct() {


		navigate('/products')
	}

	//let totalAmount = parseInt(allCart[1].productPrice) * allCart[1].quantity
	let totalAmount = 0
	for (var i = allCart.length - 1; i >= 0; i--) {
		totalAmount = totalAmount + (allCart[i].productPrice*allCart[i].quantity)


	}

	const checkout = () => {
		fetch(`https://arcane-river-38155.herokuapp.com/orders/order`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
				})

			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Successfully Checkout'
				})

				navigate('/')
				//Render the updated data using the fetchData prop
				fetchData();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
				fetchData();
			}

			})
	}





	return (
		<Container>
			<Container>
				<Wrapper>
					<Title>Your Bag</Title>
					<Top>
					<TopButton onClick={backToProduct}>CONTINUE SHOPPING</TopButton>
					<TopTexts>
						
						<TopText>ssss</TopText>
					</TopTexts>
					
					</Top>
					<Bottom>
						<Info>
						{allCart.map(cart => {
							
							const increase = () => {
								console.log(cart._id)
									//to still get the actual data from the form
									fetch(`https://arcane-river-38155.herokuapp.com/carts/${cart._id}/add`, {
									method: 'PUT',
									headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
										})

									.then(res => res.json())
									.then(data => {
										console.log(data)	
									})
								}

							const decrease = () => {
								
									//to still get the actual data from the form
									fetch(`https://arcane-river-38155.herokuapp.com/carts/${cart._id}/remove`, {
									method: 'PUT',
									headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
										})

									.then(res => res.json())
									.then(data => {
										console.log(data)
									})
								}

							const remove = () => {
								
									//to still get the actual data from the form
									fetch(`https://arcane-river-38155.herokuapp.com/carts/${cart._id}/remove`, {
									method: 'DELETE',
									headers: { 'Content-Type': 'application/json', authorization: `Bearer ${user.accessToken}`},
										})

									.then(res => res.json())
									.then(data => {
										console.log(data)
									})
								}

							return (
									
							<Product key={cart._id}>
								<ProductDetail>
									<Image src={cart.productImg}/>
									<Details>
										<ProductArtist><b>Album:</b>{cart.productName} </ProductArtist>
										<ProductArtist><b>Artist:</b>{cart.ProductArtist}  </ProductArtist>
										<ProductInfo><b>Detail:</b>{cart.productDescription}  </ProductInfo>
									</Details>
								</ProductDetail>
								<PriceDetail>
									<ProductAmountCountainer>
										<Button onClick = {decrease}><Remove/></Button>
										<ProductAmount>{cart.quantity}</ProductAmount>
										<Button onClick = {increase} ><Add/></Button>
									</ProductAmountCountainer>
									<ProductPrice>{cart.productPrice*cart.quantity}</ProductPrice>
								
									<ButtonDelete  onClick = {remove}><Delete style={{fontSize: 40}}/></ButtonDelete>
								</PriceDetail>
							</Product>
							)
								
							
						})}
							<Hr/>
						
						</Info>
						<Summary>
								<SummaryTitle>ORDER SUMMARY</SummaryTitle>
							<SummaryItem>
								<SummaryItemText>Subtotal</SummaryItemText>
								<SummaryItemPrice>{totalAmount}</SummaryItemPrice>
							</SummaryItem>

							<SummaryItem>
								<SummaryItemText>Shipping Fee</SummaryItemText>
								<SummaryItemPrice>0.00</SummaryItemPrice>
							</SummaryItem>

							<SummaryItem>
								<SummaryItemText>Discount</SummaryItemText>
								<SummaryItemPrice>0.00</SummaryItemPrice>
							</SummaryItem>

							<SummaryItem type="total">
								<SummaryItemText>Total</SummaryItemText>
								<SummaryItemPrice>{totalAmount}</SummaryItemPrice>
							</SummaryItem>

							<Button onClick = {checkout}>Checkout</Button>
						</Summary>
					</Bottom>
				</Wrapper>
			</Container>
		</Container>
	)
}

export default Cart