import React, {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddProduct({fetchData}) {

	//Add state for the forms of Course
	const [name, setName] = useState('');
	const [description , setDescription] = useState('');
	const [price , setPrice] = useState('');
	const [artist , setArtist] = useState('');
	const [img , setImg] = useState('https://s.discogs.com/images/default-release-cd.png?w=144');

	//State for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our AddCourse Modal

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	//Function for fetching thee create course in the backend
	const addProduct = (e) => {
		e.preventDefault();

		fetch('https://arcane-river-38155.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				artist: artist,
				description: description,
				price: price,
				img: img
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course successfully added'
				})

				closeAdd();
				//Render the updated data using the fetchData prop
				fetchData();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
				closeAdd();
				fetchData();
			}

			setName("")
			setDescription("")
			setPrice("")
			setArtist("")
			setImg("")
		})
	}

	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Album</Button>

			{/*Add Modal Forms*/}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<Form.Group>
							<Form.Label>Album's Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Artist</Form.Label>
							<Form.Control type="text" value={artist} onChange={e => setArtist(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Album Cover</Form.Label>
							<Form.Control placeholder="Use online image URL" type="test" value={img} onChange={e => setImg(e.target.value)}/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		)
}