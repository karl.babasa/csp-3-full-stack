import React, {useState, useEffect} from 'react'
import Product from './Product'

const UserView = ({productData}) => {
	console.log(productData)
	const [product, setProduct] = useState([])

	useEffect(() => {
		const prodArr = productData.map(product => {
			if(product.isAvailable === true) {
				return (
					<Product productProp = {product} key={product._id}/>
					)
			} else {
				return null;
			}
		})

		setProduct(prodArr)
	},[productData]) 

	return (
		<>
			{product}
		</>
	)
}

export default UserView