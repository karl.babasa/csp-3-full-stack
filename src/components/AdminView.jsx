import React, {useState, useEffect} from 'react'
import {Table} from 'react-bootstrap';

import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView(props) {

	const {productData, fetchData} = props;
	const [product, setProduct] = useState([])

	useEffect(() => {
		const productArr = productData.map(product => {
			return(
				<tr key={product._id}>
					{/*<td>{product._id}</td>*/}
					<td>{product.name}</td>
					<td>{product.artist}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isAvailable ? "text-success" : "text-danger"}>
						{product.isAvailable ? "Available" : "Unavailable"}
					</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isAvailable= {product.isAvailable} fetchData={fetchData}/></td>
				</tr>

				)
		})

		setProduct(productArr)
	}, [productData])


	return (
			<>
			<div className="text-center my-4">
				<h1>Admin Dash Board</h1>
				<AddProduct fetchData={fetchData}/>
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Album Name</th>
						<th>Artist</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan = "2">Actions</th>
					</tr>
				</thead>

				<tbody>
					{product}
				</tbody>
			</Table>
			
		</>
		)

}