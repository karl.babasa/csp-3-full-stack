import React, {useState, useEffect, useContext} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom'
//import styled from "styled-components"

export default function Login() {

	const navigate = useNavigate();

	const {setUser} = useContext(UserContext);

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		if((email !== '' && password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


	function authenticate(e) {
		e.preventDefault();

		fetch('https://arcane-river-38155.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if (data.accessToken !== undefined) {
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken})

				Swal.fire({
				title: "Yay!",
				icon: 'success',
				text: `${email} Successfully logged in`
				})
				//Getting the user's credentials
				fetch('https://arcane-river-38155.herokuapp.com/users/me', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						navigate('/products')
					} else {
						navigate('/')
					}
				})
			} else {

				Swal.fire({
				title: "Ooops!",
				icon: 'error',
				text: `Something went wrong, Check your Credentials.`
				})
			}

			setPassword('');

		})
	}

	/*const Container = styled.div`
		width: 100vw;
		height: 100vh;
		background: linear-gradient(rgba(255,255,255,0.5), rgba(255,255,255,0.5)) ,url(https://images.squarespace-cdn.com/content/v1/581374f55016e1262b711fe3/1596660742831-D82G2Y8L612DCA2PTYIP/HDP_womanholdingvinyl_TP.png?format=1000w) center;
		background-size: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
	`
	const Wrapper = styled.div`
		padding: 20px;
		width: 40%;
		background-color: white;
		border-radius: 10px;

	`
	const Form = styled.form`
		display: flex;
		flex-wrap: wrap;
		flex-direction: column;
	`
	const Title = styled.h1`
		font-size: 24px;
		font-weight: 300;
	`
	const Input = styled.input`
		flex: 1;
		min-width: 40%;
		margin: 20px 10px 0 0;
		padding: 10px;
	`
	const Button = styled.button`	
		width: 99%;
		border: none;
		padding: 15px 0px;
		background-color: teal;
		color: white;
		cursor: pointer;
		margin-top: 10px;
		border-radius: 15px;
	`*/
	
	return(

		/*<Container>
			<Wrapper>
				<Title>SIGN IN</Title>
				<Form onSubmit={(e) => authenticate(e)}>
					<Input placeholder = "Email" type="email" required value={email} onChange={e => setEmail(e.target.value)}/>
					<Input placeholder = "Password" type="text" required value={password} onChange={e => setPassword(e.target.value)}/>
					<Button type="submit">SUBMIT</Button>
				</Form>
			</Wrapper>
		</Container>*/
	<Container>
		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter Email Address"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="my-3">Login</Button>

				:

				<Button variant="danger" type="submit" className="my-3" disabled>Login</Button>
			}
 		</Form>
 	</Container>
 		
		)
}
