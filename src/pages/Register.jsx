import React, {useState, useEffect} from 'react';
import {Form, Button} from	'react-bootstrap';
import {Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
//import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom'
//import styled from "styled-components"

export default function	Register() {

	//const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password1 , setPassword1] = useState('');
	const [password2 , setPassword2] = useState('');
	const [firstName , setFirstName] = useState('');
	const [lastname , setLastName] = useState('');
	const [mobileNumber , setMobileNumber] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== "" && lastname !== "" && mobileNumber !== "") && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastname, mobileNumber])

	function registerUser(e) {
		e.preventDefault()

		fetch('https://arcane-river-38155.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1,
				firstName: firstName,
				lastName: lastname,
				mobileNumber: mobileNumber

			})

		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			Swal.fire({
			title: "Yay!",
			icon: 'success',
			text: `Successfully Registered an Account`
			})
			
			navigate('/login')

			setEmail('');
			setPassword1('');
			setPassword2('');
			setFirstName('');
			setLastName('');
			setMobileNumber('');
		})
	}

	return(
	<Container>
			<Form onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
					type="email"
					placeholder="Enter Email Address"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
					type="text"
					placeholder="First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
					type="text"
					placeholder="Last Name"
					required
					value={lastname}
					onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control 
					type="text"
					placeholder="Mobile Number"
					required
					value={mobileNumber}
					onChange={e => setMobileNumber(e.target.value)}/>
				</Form.Group>

				{isActive ?
					<Button variant="primary" type="submit" className="my-3">Create</Button>

					:

					<Button variant="danger" type="submit" className="my-3" disabled>Create</Button>
				}
			</Form>
	</Container>
		)
}