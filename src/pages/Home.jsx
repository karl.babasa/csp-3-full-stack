import React from 'react';
import Slider from '../components/Slider'
import Products from '../components/Products'
//import Navbar from '../components/Navbar'
import Footer from '../components/Footer'

const Home = () => {
	return (
		<div>
		
			<Slider/>
    		<Products/>
 			<Footer/> 
		</div>
	)
}

export default Home